package storage

import (
	"branch/genproto/organization_service"
	"context"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Employee() EmployeeRepoI
	Supplier() SupplierRepoI
	Market() MarketRepoI
}

type BranchRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateBranchRequest) (resp *organization_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.BranchPrimaryKey) (resp *organization_service.Branch, err error)
	GetAll(ctx context.Context, req *organization_service.GetListBranchRequest) (resp *organization_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateBranchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error
}

type EmployeeRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateEmployee) (resp *organization_service.EmployeePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *organization_service.Employee, err error)
	GetAll(ctx context.Context, req *organization_service.GetListEmployeeRequest) (resp *organization_service.GetListEmployeeResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateEmployee) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) error
}

type SupplierRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateSupplierRequest) (resp *organization_service.SupplierPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.SupplierPrimaryKey) (resp *organization_service.Supplier, err error)
	GetAll(ctx context.Context, req *organization_service.GetListSupplierRequest) (resp *organization_service.GetListSupplierResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateSupplierRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.SupplierPrimaryKey) error
}

type MarketRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateMarket) (resp *organization_service.MarketPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.MarketPrimaryKey) (resp *organization_service.Market, err error)
	GetAll(ctx context.Context, req *organization_service.GetListMarketRequest) (resp *organization_service.GetListMarketResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateMarket) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.MarketPrimaryKey) error
}
