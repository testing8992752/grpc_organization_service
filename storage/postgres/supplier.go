package memory

import (
	"branch/genproto/organization_service"
	"branch/packages/helper"
	"branch/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type SupplierRepo struct {
	db *pgxpool.Pool
}

func NewSupplierRepo(db *pgxpool.Pool) storage.SupplierRepoI {
	return &SupplierRepo{
		db: db,
	}
}

func (c *SupplierRepo) Create(ctx context.Context, req *organization_service.CreateSupplierRequest) (resp *organization_service.SupplierPrimaryKey, err error) {
	var id = uuid.New()

	query := `
		INSERT INTO "supplier" (
			id,
			name,
			phone_number,
			is_active,
			updated_at
		) VALUES ($1, $2, $3, $4, now())
	`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.PhoneNumber,
		req.IsActive,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.SupplierPrimaryKey{Id: id.String()}, nil
}

func (c *SupplierRepo) GetByPKey(ctx context.Context, req *organization_service.SupplierPrimaryKey) (resp *organization_service.Supplier, err error) {
	query := `
		SELECT
			id,
			name,
			phone_number,
			is_active,
			created_at,
			updated_at
		FROM "supplier"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		name        sql.NullString
		phoneNumber sql.NullString
		isActive    sql.NullBool
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&phoneNumber,
		&isActive,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &organization_service.Supplier{
		Id:          id.String,
		Name:        name.String,
		PhoneNumber: phoneNumber.String,
		IsActive:    isActive.Bool,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *SupplierRepo) GetAll(ctx context.Context, req *organization_service.GetListSupplierRequest) (resp *organization_service.GetListSupplierResponse, err error) {
	resp = &organization_service.GetListSupplierResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			phone_number,
			is_active,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "supplier"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			phoneNumber sql.NullString
			isActive    sql.NullBool
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&phoneNumber,
			&isActive,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Suppliers = append(resp.Suppliers, &organization_service.Supplier{
			Id:          id.String,
			Name:        name.String,
			PhoneNumber: phoneNumber.String,
			IsActive:    isActive.Bool,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *SupplierRepo) Update(ctx context.Context, req *organization_service.UpdateSupplierRequest) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		    "supplier"
		SET
			name = :name,
			phone_number = :phone_number,
			is_active = :is_active,
			updated_at = now()
		WHERE
			id = :id
	`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"phone_number": req.GetPhoneNumber(),
		"is_active":    req.GetIsActive(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SupplierRepo) Delete(ctx context.Context, req *organization_service.SupplierPrimaryKey) error {
	query := `DELETE FROM "supplier" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
