package memory

import (
	"branch/genproto/organization_service"
	"branch/packages/helper"
	"branch/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) storage.BranchRepoI {
	return &BranchRepo{
		db: db,
	}
}

func (c *BranchRepo) Create(ctx context.Context, req *organization_service.CreateBranchRequest) (resp *organization_service.BranchPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "branch" (
				id,
				branch_code,
				name,	
				address,
				updated_at
			) VALUES ($1, $2, $3, $4,  now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchCode,
		req.Name,
		req.Address,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.BranchPrimaryKey{Id: id.String()}, nil
}

func (c *BranchRepo) GetByPKey(ctx context.Context, req *organization_service.BranchPrimaryKey) (resp *organization_service.Branch, err error) {

	query := `
		SELECT
			id,
			branch_code, 
			name,	
			address,
			created_at,
			updated_at
		FROM "branch"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branch_code sql.NullString
		name        sql.NullString
		address     sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_code,
		&name,
		&address,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &organization_service.Branch{
		Id:         id.String,
		BranchCode: branch_code.String,
		Name:       name.String,
		Address:    address.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *BranchRepo) GetAll(ctx context.Context, req *organization_service.GetListBranchRequest) (resp *organization_service.GetListBranchResponse, err error) {

	resp = &organization_service.GetListBranchResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_code, 
			name,	
			address,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "branch"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branch_code sql.NullString
			name        sql.NullString
			address     sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_code,
			&name,
			&address,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Branches = append(resp.Branches, &organization_service.Branch{
			Id:         id.String,
			BranchCode: branch_code.String,
			Name:       name.String,
			Address:    address.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *BranchRepo) Update(ctx context.Context, req *organization_service.UpdateBranchRequest) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "branch"
			SET
				branch_code = :branch_code,
				name = :name,
				address = :address,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"branch_code": req.GetBranchCode(),
		"id":          req.GetId(),
		"name":        req.GetName(),
		"address":     req.GetAddress(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BranchRepo) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error {

	query := `DELETE FROM "branch" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
