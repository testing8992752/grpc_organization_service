package memory

import (
	"branch/genproto/organization_service"
	"branch/packages/helper"
	"branch/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type EmployeeRepo struct {
	db *pgxpool.Pool
}

func NewEmployeeRepo(db *pgxpool.Pool) storage.EmployeeRepoI {
	return &EmployeeRepo{
		db: db,
	}
}

func (c *EmployeeRepo) Create(ctx context.Context, req *organization_service.CreateEmployee) (resp *organization_service.EmployeePrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "employee" (
				id,
				first_name,
				last_name,
				phone,
				login,
				password,
				market_id,
				user_type,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.Password,
		req.SalePointId,
		req.UserType,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.EmployeePrimaryKey{Id: id.String()}, nil
}

func (c *EmployeeRepo) GetByPKey(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *organization_service.Employee, err error) {

	var (
		id        sql.NullString
		firstName sql.NullString
		lastName  sql.NullString
		phone     sql.NullString
		login     sql.NullString
		password  sql.NullString
		salePoint sql.NullString
		userType  sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	var (
		where    string
		argument string
	)

	if len(req.Id) > 0 {
		where = " WHERE id = $1"
		argument = req.Id
	} else if len(req.Login) > 0 {
		where = " WHERE login = $1"
		argument = req.Login
	}
	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone,
			login,
			password,
			market_id,
			user_type,
			created_at,
			updated_at
		FROM "employee"
	`

	query += where

	err = c.db.QueryRow(ctx, query, argument).Scan(
		&id,
		&firstName,
		&lastName,
		&phone,
		&login,
		&password,
		&salePoint,
		&userType,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &organization_service.Employee{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		Phone:       phone.String,
		Login:       login.String,
		Password:    password.String,
		SalePointId: salePoint.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *EmployeeRepo) GetAll(ctx context.Context, req *organization_service.GetListEmployeeRequest) (resp *organization_service.GetListEmployeeResponse, err error) {

	resp = &organization_service.GetListEmployeeResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone,
			login,
			password,
			market_id,
			user_type,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "employee"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			firstName   sql.NullString
			lastName    sql.NullString
			phone       sql.NullString
			login       sql.NullString
			password    sql.NullString
			salePointID sql.NullString
			userType    sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&firstName,
			&lastName,
			&phone,
			&login,
			&password,
			&salePointID,
			&userType,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Employees = append(resp.Employees, &organization_service.Employee{
			Id:          id.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			Phone:       phone.String,
			Login:       login.String,
			Password:    password.String,
			SalePointId: salePointID.String,
			UserType:    userType.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *EmployeeRepo) Update(ctx context.Context, req *organization_service.UpdateEmployee) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "employee"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone = :phone,
				login = :login,
				password = :password,
				market_id = :market_id,
				user_type = :user_type,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"first_name": req.GetFirstName(),
		"last_name":  req.GetLastName(),
		"phone":      req.GetPhone(),
		"login":      req.GetLogin(),
		"password":   req.GetPassword(),
		"market_id":  req.GetSalePointId(),
		"user_type":  req.GetUserType(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *EmployeeRepo) Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) error {

	query := `DELETE FROM "employee" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
