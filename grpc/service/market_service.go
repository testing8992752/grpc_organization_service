package service

import (
	"branch/config"
	"branch/genproto/organization_service"
	"branch/grpc/client"
	"branch/packages/logger"
	"branch/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MarketService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedMarketServiceServer
}

func NewMarketService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *MarketService {
	return &MarketService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *MarketService) Create(ctx context.Context, req *organization_service.CreateMarket) (resp *organization_service.Market, err error) {

	i.log.Info("---CreateMarket------>", logger.Any("req", req))

	pKey, err := i.strg.Market().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateMarket->Market->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Market().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MarketService) GetByID(ctx context.Context, req *organization_service.MarketPrimaryKey) (resp *organization_service.Market, err error) {

	i.log.Info("---GetMarketByID------>", logger.Any("req", req))

	resp, err = i.strg.Market().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMarketByID->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MarketService) GetList(ctx context.Context, req *organization_service.GetListMarketRequest) (resp *organization_service.GetListMarketResponse, err error) {

	i.log.Info("---GetMarkets------>", logger.Any("req", req))

	resp, err = i.strg.Market().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMarkets->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MarketService) Update(ctx context.Context, req *organization_service.UpdateMarket) (resp *organization_service.Market, err error) {

	i.log.Info("---UpdateMarket------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Market().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateMarket--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Market().GetByPKey(ctx, &organization_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *MarketService) Delete(ctx context.Context, req *organization_service.MarketPrimaryKey) (resp *organization_service.Emptys, err error) {

	i.log.Info("---DeleteMarket------>", logger.Any("req", req))

	err = i.strg.Market().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &organization_service.Emptys{}, nil
}
