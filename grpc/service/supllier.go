package service

import (
	"branch/config"
	"branch/genproto/organization_service"
	"branch/grpc/client"
	"branch/packages/logger"
	"branch/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SupplierService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedSupplierServiceServer
}

func NewSupplierService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SupplierService {
	return &SupplierService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (s *SupplierService) Create(ctx context.Context, req *organization_service.CreateSupplierRequest) (resp *organization_service.Supplier, err error) {
	s.log.Info("---CreateSupplier------>", logger.Any("req", req))

	pKey, err := s.strg.Supplier().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateSupplier->Supplier->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = s.strg.Supplier().GetByPKey(ctx, pKey)
	if err != nil {
		s.log.Error("!!!GetByPKeySupplier->Supplier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (s *SupplierService) GetByID(ctx context.Context, req *organization_service.SupplierPrimaryKey) (resp *organization_service.Supplier, err error) {
	s.log.Info("---GetSupplierByID------>", logger.Any("req", req))

	resp, err = s.strg.Supplier().GetByPKey(ctx, req)
	if err != nil {
		s.log.Error("!!!GetSupplierByID->Supplier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (s *SupplierService) GetList(ctx context.Context, req *organization_service.GetListSupplierRequest) (resp *organization_service.GetListSupplierResponse, err error) {
	s.log.Info("---GetSuppliers------>", logger.Any("req", req))

	resp, err = s.strg.Supplier().GetAll(ctx, req)
	if err != nil {
		s.log.Error("!!!GetSuppliers->Supplier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (s *SupplierService) Update(ctx context.Context, req *organization_service.UpdateSupplierRequest) (resp *organization_service.Supplier, err error) {
	s.log.Info("---UpdateSupplier------>", logger.Any("req", req))

	rowsAffected, err := s.strg.Supplier().Update(ctx, req)

	if err != nil {
		s.log.Error("!!!UpdateSupplier--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = s.strg.Supplier().GetByPKey(ctx, &organization_service.SupplierPrimaryKey{Id: req.Id})
	if err != nil {
		s.log.Error("!!!GetSupplier->Supplier->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (s *SupplierService) Delete(ctx context.Context, req *organization_service.SupplierPrimaryKey) (resp *organization_service.Emptyr, err error) {
	s.log.Info("---DeleteSupplier------>", logger.Any("req", req))

	err = s.strg.Supplier().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteSupplier->Supplier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &organization_service.Emptyr{}, nil
}
