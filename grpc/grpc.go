package grpc

import (
	"branch/config"
	"branch/genproto/organization_service"
	"branch/grpc/client"
	"branch/grpc/service"
	"branch/packages/logger"
	"branch/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	organization_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	organization_service.RegisterEmployeeServiceServer(grpcServer, service.NewEmployeeService(cfg, log, strg, srvc))
	organization_service.RegisterSupplierServiceServer(grpcServer, service.NewSupplierService(cfg, log, strg, srvc))
	organization_service.RegisterMarketServiceServer(grpcServer, service.NewMarketService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
