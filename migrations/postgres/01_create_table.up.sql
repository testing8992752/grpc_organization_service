
CREATE TABLE IF NOT EXISTS "branch" (
  "id" uuid PRIMARY KEY,
  "branch_code" varchar(2), 
  "name" varchar,
  "address" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "market" (
  "id" uuid PRIMARY KEY,
  "branch_id" uuid REFERENCES "branch"("id"),
  "name" varchar,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "employee" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone" varchar,
  "login" varchar,
  "password" varchar,
  "user_type" varchar,
  "market_id" uuid REFERENCES "market"("id"),
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "supplier" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "phone_number" varchar,
  "is_active" boolean DEFAULT true,
  "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamp DEFAULT CURRENT_TIMESTAMP
);
